/*
 * claws-mail-theme-reloader -- Theme reloader plugin for Claws Mail
 * Copyright (C) 2017 Ricardo Mones and the Claws Mail Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>

#define GETTEXT_PACKAGE "claws-mail-theme-reloader"
#include <glib/gi18n-lib.h>

#include <gtk/gtk.h>

#include "defs.h"
#include "version.h"
#include "plugin.h"
#include "mainwindow.h"
#include "menu.h"
#include "alertpanel.h"
#include "stock_pixmap.h"
#include "prefs_common.h"
#include "compose.h"
#include "addr_compl.h"

#define PLUGIN_NAME (_("Theme Reloader"))
#define PLUGIN_VERSION "1.0.1"

static void update_reload_themes_menu(void);

GtkWidget *themes_menuitem;
MainWindow *mainwin;
static guint main_menu_id = 0;

const gchar *plugin_name(void) { return PLUGIN_NAME; }
const gchar *plugin_version(void) { return PLUGIN_VERSION; }
const gchar *plugin_type(void) { return "GTK2"; }
const gchar *plugin_licence(void) { return "GPL3+"; }

struct PluginFeature *plugin_provides(void)
{
	static struct PluginFeature features[] = 
		{ {PLUGIN_UTILITY, N_("Theme Reloader")},
		  {PLUGIN_NOTHING, NULL}};
	return features;
}

const gchar *plugin_desc(void)
{
	return _("This plugin lets you quickly reload current theme "
		"or switch to any of the installed themes. It is meant "
		"as a tool to help speed up theme development.");
}

static GtkActionEntry reloader_main_menu[] = {
	{"Tools/ReloadTheme", NULL, N_("Reload Theme"), NULL, NULL,
		G_CALLBACK(gtk_false)}
};

gint plugin_init(gchar **error)
{
	if (!check_plugin_version(MAKE_NUMERIC_VERSION(3,14,1,96),
			VERSION_NUMERIC, PLUGIN_NAME, error))
		return -1;

	mainwin = mainwindow_get_mainwindow();

	gtk_action_group_add_actions(mainwin->action_group, reloader_main_menu,
			G_N_ELEMENTS(reloader_main_menu), mainwin);

	MENUITEM_ADDUI_ID_MANAGER(mainwin->ui_manager,
			"/Menu/Tools", "ReloadTheme", "Tools/ReloadTheme",
			GTK_UI_MANAGER_MENUITEM, main_menu_id);

	debug_print("Plugin Reloader plug-in loaded\n");

	themes_menuitem = gtk_ui_manager_get_widget(mainwin->ui_manager,
			"/Menu/Tools/ReloadTheme");
	g_signal_connect(G_OBJECT(themes_menuitem), "activate",
			G_CALLBACK(update_reload_themes_menu), NULL);

	update_reload_themes_menu();

	return 0;
}

gboolean plugin_done(void)
{
	MainWindow *mainwin = mainwindow_get_mainwindow();

	if (mainwin == NULL)
		return TRUE;

	MENUITEM_REMUI_MANAGER(mainwin->ui_manager, mainwin->action_group,
			"Tools/ReloadTheme", main_menu_id);
	main_menu_id = 0;

	debug_print("Theme Reloader plugin unloaded\n");

	return TRUE;
}

static void reload_theme_cb(GtkAction *action, gpointer data)
{
	gchar *theme = data;
	gchar *short_name = g_path_get_basename((const gchar *)theme);
	PrefsCommon *prefs = prefs_common_get_prefs();

	if (strcmp(prefs->pixmap_theme_path, theme) == 0) {
		debug_print("reloading theme '%s'\n", short_name);
		stock_pixmap_invalidate_all_icons();
	} else {
		debug_print("switching theme to '%s'\n", short_name);
		g_free(prefs->pixmap_theme_path);
		prefs->pixmap_theme_path = g_strdup(theme);
	}
	main_window_reflect_prefs_all_real(TRUE);
	compose_reflect_prefs_pixmap_theme();
	addrcompl_reflect_prefs_pixmap_theme();

	g_free(short_name);
}

static void update_reload_themes_menu(void)
{
	GList *themes_list = stock_pixmap_themes_list_new();
	GList *cur = NULL;
	GtkWidget *menu = gtk_menu_new();
	GtkUIManager *ui_manager = mainwin->ui_manager;
	static const gchar *accel_group = "<ReloadTheme>/";
	gchar *accel_path = NULL;

	gtk_menu_set_accel_group(GTK_MENU(menu),
			gtk_ui_manager_get_accel_group(ui_manager));

	gtk_menu_item_set_submenu(GTK_MENU_ITEM(themes_menuitem), NULL);

	for (cur = themes_list; cur != NULL; cur = g_list_next(cur)) {
		gchar *theme_name = cur->data;
		gchar *short_name = NULL;
		GtkWidget *item = NULL;

		if (!strcmp(theme_name, DEFAULT_PIXMAP_THEME))
			short_name = g_strdup(_("Default internal theme"));
		else
			short_name = g_path_get_basename((const gchar *)theme_name);
		item = gtk_menu_item_new_with_label(short_name);

		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		g_signal_connect(G_OBJECT(item), "activate",
				G_CALLBACK(reload_theme_cb),
				g_strdup(theme_name));

		accel_path = g_strconcat(accel_group, short_name, NULL);
		gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item), accel_path);
		g_free(accel_path);
		g_free(short_name);
	}

	gtk_widget_show_all(menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(themes_menuitem), menu);

	stock_pixmap_themes_list_free(themes_list);
}

